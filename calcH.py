import math
import sys

minH =  float(sys.argv[1])
maxH = float(sys.argv[2])
r = float(sys.argv[3])

base = math.sqrt(3/4) * r
n = round(minH / base)

if n * base < minH:
    n += 1

while n * base <= maxH:
    print(base*n)
    n += 1
