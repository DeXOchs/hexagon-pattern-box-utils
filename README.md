# Hexagon Pattern Box Utils

Utilities to parametrically compute measurements for physical, rectangular boxes (f. ex. wooden boxes) with a decorating hexagon pattern on the outside and fixed measurements of its inside (fixed inner width and length for, e.g., standardized inlets and trays). Different values are computed along some constraints and by varying the thickness of the material (equal to varying the width-length-ratio of the box) and the size of the hexagons.

## Intuitive Problem and Idea

You want to construct or build a box (a rectangular frame) and you have a fixed inner width `w_i` and length `l_i`. The box will have a certain thickness `s` and a hexagon can be of size `r`.

![Overview](docs/overview.png)

On the outside, the box will have a stylish heaxon-shaped pattern. It would be great, if the pattern will continue all around the box and through all edges, so a hexagon split by an edge on the first side will be continued on the next side as if the edge isn't there.

![Hexagon Split](docs/hexagon_split.png)

The problem is, that for a certain width-length-ratio the size of a single hexagon isn't trivial to set to meet all the constraints (f. ex. the continuous pattern all around the box).

 - With the `calcSR` script you can compute the thickness `s` and the hexagon size `r` to derive the outer box width `w_b` and length `l_b`. See detailed documentation [here](docs/calc_sr.md).
 - With the `calcH` script you can then compute the possible height steps of the box so it aligns harmonically to the size of a hexagon. See detailed documentation [here](docs/calc_h.md).

Combined, you can find an unambiguous alignment of the hexagon pattern.


## Scripts

Call the Python3 scripts via command line with the following parameters:
  
| Script | Input Parameters (ordered) | Output |
|--|--|--|
| calcSR | `w_i l_i minS maxS minR maxR granularityS granularityR` | list of possible solutions `(w_b, l_b, s, r, splitStrategy, error)` |
| calcH | `minH maxH r` | list of possible, valid heights `h` |

Read the detailed documentation of each script ([calcSR](docs/calc_sr.md), [calcH](docs/calc_h.md)) to understand the parameters and the output.