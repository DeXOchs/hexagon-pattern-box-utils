# calcSR

This page covers the basics, the constraints, the problem and the solution documentation of the `calcSR.py` script. Be sure to read the problem intuition, found in the readme, first.

## Basics

###  Measurements
The measurements are named as follows:

- inner width `w_i` and length `l_i`
- outer/ box width `w_b` and length `l_b`
- material thickness `s`
- a regular hexagon with radius `r` yields to width `2r` and height `sqrt(3)*r`

![Measurements](measurements.png)

### Pattern

For the pattern, its fixed that the front side of the box is mirror-symmetrical with the hexagons split exactly in the middle, on the left and right edge of the front side (`w_b / 3r` must be an integer). Therefor, the left and right sides are starting and ending the pattern exactly the same, pairwise. The rear side is comparable to the front side and must align the pattern to the endings of the left and right side.

![Hexagon Split](hexagon_split.png)

### Split Strategies

The left and right side can end differently to how they start, depending on their length (`l_b`). This is called the Split Strategy:

- If the left and right sides length `l_b` is a multiple of `3r` (`l_b / 3r` is an integer), they end the pattern the same way they started it, so they are also mirror-symmetrical. The rear side is then exactly equal to the front side. This is called the `SYM` Split Strategy.
- If the left and right sides length `l_b` is a multiple of `3/2 r`, they end the pattern with an offset of `3/2 r` to how they started it and are therefor not mirror-symmetrical. The hexagons are split in the middle nevertheless. The rear side pattern is not equal to the front side (its shifted by `3/2 r` compared to the front side). This is called the `OFFSET` Split Strategy.

![Split Strategies](split_strategies.png)

## 2 Constraints

To solve the problem, basically two constraints have to be fulfilled:

1. `w_b / 3r` is an integer (front side is mirror-symmetrical)
2. `l_b / 3r` (`SYM`) or `2l_b / 3r` (`OFFSET`) is an integer (left and right sides adopt one of both Split Strategies)

Then, implicitly given, if first and second constraint are fulfilled: `(l_b + w_b) / r` is an integer (pattern is continuous all around the box).

## (Optimisation) Problem 

As one can set material thickness `s` and hexagon size `r` freely, the goal is to find a combination of `s` and `r`, so that the left and right sides adopt a Split Strategy as good as possible, e.g., `l_b / 3r` or `2l_b / 3r` converge as nearly to an integer as possible. 

The goal is to minimise the error
`round(l_b / 3r) – l_b / 3r` (`SYM`)  or
`round(2l_b / 3r) – l_b / 3r` (`OFFSET`).

## Solution Approach

The optimisation problem can be solved in different ways, but basically is computationally hard.

### Problem Reduction

It can be reduced to polynomial time by introducing intervals for material thickness `s` and hexagon size `r` and a granularity for `s` and `r` (`granularityS`, `granularityR`). The complexity then is defined by the number of possible combinations of `s` and `r`, reduced from infinity to 
`((maxS–minS)*granularityS) * (maxR–minR)*granularityR)`.

The `calcSR` script brute forces `s` and `r` by sorting each combination of `s` and `r` by its error ascendingly.

### Solution Intuition

The optimal solution can be found for a material thickness `s` yielding box measurements `w_b` and `l_b`, which are an integer multiple of `3r` (`SYM`) or `3/2 r` (`OFFSET`). Then the error is `0`.

Defining the intervals
`[minS, maxS] = [10, 11]`,
`[minR, maxR] = [10, 20]` (f. ex. in millimeters),
and the granularities
`granularityS = 10`, `granularityR = 10`
the script has to try
`((11-10)*10) * ((20-10)*10) = 1000` combinations, which should easily be computable.

The interval for the material thickness `s` can be your available raw materials (f. ex. wooden boards). The interval for the hexagon size `r` can be your personal, optical preferences (big hexagons or small ones). The granularities can be aligned to how precisely you can work with your tools.
