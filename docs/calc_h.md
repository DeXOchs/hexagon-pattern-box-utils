# calcH

This page covers the documentation of the `calcH.py` script. Be sure to read the problem intuition, found in the readme, first.

## Basics

A regular hexagon with size `r` is of height `sqrt(3)*r`. The height of a box `h` should be in the style of not splitting a hexagon anywhere else than at the top/ bottom or exactly in the middle for optical reasons.

Therefor, `h` must be an integer multiple of `sqrt(3/4)*r`.

![Measurements](measurements.png)
![Measurements](hexagon_height.png)

## Solution

The `calcH.py` script takes an interval `[minH, maxH]` and the hexagon size `r` and computes all possible, valid values for the box height `h` inside that interval.

If you set
`[minH, maxH] = [100, 130]` and `r = sqrt(3/4)*12`,
you should get the results
`[108, 117, 126]`.

Of course, some combinations do not have any results.
