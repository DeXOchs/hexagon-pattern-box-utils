w_i = 113.0
l_i = 174.0

minR = 5
maxR = 40

minS = 10
maxS = 26

granularityR = 100;
granularityS = 10;

solutions = []; # list[stretchM_, s, r, m, m_, w_b, l_b, stratM_]

for sIter in range(minS * granularityS, maxS * granularityS):
    s = sIter / granularityS

    w_b = w_i + 2*s
    l_b = l_i + 2*s

    for rIter in range(minR * granularityR, maxR * granularityR):
        r = rIter / granularityR

        m = w_b / (3*r)
        condM = m.is_integer()

        if condM:
            stratsM_ = [
                ["SYM", l_b / (3*r)],
                ["OFFSET", (2*s) / (3*r)]
            ]

            for [stratName, m_] in stratsM_:

                solutions.append([
                    abs(round(m_) - m_),
                    s,
                    r,
                    m,
                    m_,
                    w_b,
                    l_b,
                    stratName
                ])

solutions.sort(key=(lambda item: item[0]))

print("Top 10 solutions:")
for [stretchM_, s, r, m, m_, w_b, l_b, stratM_] in solutions[:(min(10, len(solutions)))]:
    #print([stretchM_, s, r, m, m_, w_b, l_b, stratM_])
    print("w_b", w_b, "and l_b", l_b, "constructed with s", s, "r", r, "yield to m", m, "and m_", m_, "with strat", stratM_)
